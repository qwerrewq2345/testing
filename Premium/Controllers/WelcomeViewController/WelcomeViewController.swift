//
//  ViewController.swift
//  Premium
//
//  Created by Иван Климов on 22.05.2023.
//

import UIKit
import SnapKit

final class WelcomeViewController: UIViewController {
    
    // MARK: - UI Elements
    private lazy var backgroundView = BackgroundView()
    private lazy var centralInfoImageView = UIImageView()
    private lazy var welcomeLabelImageView = UIImageView()
    private lazy var continueButton = ContinueButton()
    private lazy var termsOfUseAndPrivacyPolicyButtons = TermsOfUseAndPrivacyPolicyButtons()
    
    // MARK: - Life cycle
    override func loadView() {
        view = UIView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Navigation
private extension WelcomeViewController {
    @objc func nextVC() {
        let rootVC = PrivacyPolicyViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
    @objc func openPrivacyPolicy() {
        termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.backgroundColor = .systemIndigo
        UIView.animate(withDuration: 0.01) {
            self.termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.backgroundColor = .clear
        }
        print("openPrivacyPolicy")
    }
    @objc func openTermsOfUse() {
        termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.backgroundColor = .systemIndigo
        UIView.animate(withDuration: 0.01) {
            self.termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.backgroundColor = .clear
        }
        print("openTermsOfUse")
    }
}

// MARK: - Layout Setup

private extension WelcomeViewController {
    func setupView() {
        view.addSubview(backgroundView)
        
        view.addSubview(welcomeLabelImageView)
        welcomeLabelImageView.image = SF.Image.welcomeLabelImageViewImage
        welcomeLabelImageView.contentMode = .scaleAspectFit
        
        view.addSubview(centralInfoImageView)
        centralInfoImageView.image = SF.Image.centralInfoImageViewImage
        
        view.addSubview(continueButton)
        continueButton.continueButton.addTarget(self, action: #selector(nextVC), for: .touchUpInside)
        
        view.addSubview(termsOfUseAndPrivacyPolicyButtons)
        termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.addTarget(self, action: #selector(openPrivacyPolicy), for: .touchUpInside)
        termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.addTarget(self, action: #selector(openTermsOfUse), for: .touchUpInside)
    }
    
    func setupConstraints(){
        backgroundView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        
        welcomeLabelImageView.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().inset(view.frame.height / 10.5)
            $0.height.equalTo(view.frame.height / 9)
            $0.width.equalTo(view.frame.width / 1.4)
        }
        
        centralInfoImageView.snp.makeConstraints{
            $0.width.equalTo(view.frame.width / 1.4)
            $0.height.equalTo(view.frame.height / 2.5)
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().inset(view.frame.height / 3.6)
        }
        
        continueButton.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(view.frame.height / 5.5)
        }

        termsOfUseAndPrivacyPolicyButtons.setupConstraints(superView: view, continueButton: continueButton)
    }
}
