//
//  PrivacyPolicyButton.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit
import SnapKit

final class TermsOfUseAndPrivacyPolicyButtons: UIView {

    // MARK: - UI Elements
    private lazy var container = UIImageView()
    lazy var termsOfUseButton = UIButton()
    lazy var privacyPolicyButton = UIButton()
    
    //MARK: - Init
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Setup Layout
private extension TermsOfUseAndPrivacyPolicyButtons {
    func setupView() {
        addSubview(container)
        container.image = SF.Image.privacyPolicyButtonImage
        container.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        addSubview(termsOfUseButton)
        termsOfUseButton.snp.makeConstraints {
            $0.left.top.bottom.equalToSuperview()
            $0.width.lessThanOrEqualTo(90)
        }
        
        addSubview(privacyPolicyButton)
        privacyPolicyButton.snp.makeConstraints {
            $0.right.top.bottom.equalToSuperview()
            $0.width.lessThanOrEqualTo(90)
            $0.left.equalTo(termsOfUseButton.snp.right)
        }
    }
}

//MARK: - Setup Constraints
extension TermsOfUseAndPrivacyPolicyButtons {
    func setupConstraints(superView: UIView, continueButton: ContinueButton) {
        self.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(continueButton.snp.bottomMargin).inset(-15)
            $0.width.equalTo(superView.frame.width / 2.3 )
            $0.height.equalTo(superView.frame.height / 49)
        }
    }
}
