//
//  backgroundView.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit
import SnapKit

final class BackgroundView: UIView{
    
    // MARK: - UI Elements
    private lazy var backgroundView = UIImageView()
    
    //MARK: - Init
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Setup Layout
private extension BackgroundView{
    func setupView(){
        addSubview(backgroundView)
        backgroundView.image = SF.Image.backgroundImage
        backgroundView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
}
