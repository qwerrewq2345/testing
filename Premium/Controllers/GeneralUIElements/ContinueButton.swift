//
//  ContinueButton.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit
import SnapKit

final class ContinueButton: UIButton{
    
    // MARK: - UI Elements
    lazy var continueButton = UIButton()
    
    //MARK: - Init
    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Setup Layout
private extension ContinueButton{
    func setupView(){
        addSubview(continueButton)
        continueButton.setImage(SF.Image.continueButtonImage, for: .normal)
        continueButton.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
}

