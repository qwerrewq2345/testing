//
//  PrivacyPolicyViewController.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit
import SnapKit

final class PrivacyPolicyViewController: UIViewController {

    // MARK: - UI Elements
    private lazy var backgroundView = BackgroundView()
    private lazy var privacyPolicyLabelImageView = UIImageView()
    private lazy var textPrivacyPolicyLabel = UILabel()
    private lazy var viewBeforePrivacyPolicyLabel = UIView()
    private lazy var continueButton = ContinueButton()
    private lazy var termsOfUseAndPrivacyPolicyButtons = TermsOfUseAndPrivacyPolicyButtons()
    
    // MARK: - Life cycle
    override func loadView() {
        view = UIView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }

    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Navigation
private extension PrivacyPolicyViewController {
    @objc func openPrivacyPolicy() {
        termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.backgroundColor = .systemIndigo
        UIView.animate(withDuration: 0.01) {
            self.termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.backgroundColor = .clear
        }
        print("openPrivacyPolicy")
    }
    @objc func openTermsOfUse() {
        termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.backgroundColor = .systemIndigo
        UIView.animate(withDuration: 0.01) {
            self.termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.backgroundColor = .clear
        }
        print("openTermsOfUse")
    }
    
    @objc func nextVC() {
        let rootVC = PremiumViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: false)
    }
}

// MARK: - Layout Setup
private extension PrivacyPolicyViewController {
    func setupView() {
        view.addSubview(backgroundView)
        
        view.addSubview(privacyPolicyLabelImageView)
        privacyPolicyLabelImageView.image = SF.Image.privacyPolicyLabelImageView
        privacyPolicyLabelImageView.contentMode = .scaleAspectFill
        
        view.addSubview(viewBeforePrivacyPolicyLabel)
        viewBeforePrivacyPolicyLabel.backgroundColor = SF.Color.textPrivacyPolicyLabelbackgroundColor
        viewBeforePrivacyPolicyLabel.layer.masksToBounds = true
        viewBeforePrivacyPolicyLabel.layer.cornerRadius = 10
        
        let blurEffect = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = textPrivacyPolicyLabel.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.alpha = 0.8
        viewBeforePrivacyPolicyLabel.addSubview(blurView)
        
        viewBeforePrivacyPolicyLabel.addSubview(textPrivacyPolicyLabel)
        textPrivacyPolicyLabel.text = SF.Text.textPrivacyPolicyLabelText
        textPrivacyPolicyLabel.backgroundColor = .clear
        textPrivacyPolicyLabel.numberOfLines = 0

        textPrivacyPolicyLabel.textColor = SF.Color.textPrivacyPolicyLabelTextColor
        textPrivacyPolicyLabel.font = SF.Font.textPrivacyPolicyLabelFont
        
        view.addSubview(continueButton)
        continueButton.continueButton.addTarget(self, action: #selector(nextVC), for: .touchUpInside)
        
        view.addSubview(termsOfUseAndPrivacyPolicyButtons)
        termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.addTarget(self, action: #selector(openPrivacyPolicy), for: .touchUpInside)
        termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.addTarget(self, action: #selector(openTermsOfUse), for: .touchUpInside)
    }
    
    func setupConstraints(){
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        privacyPolicyLabelImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().inset(view.frame.height / 10.5)
            $0.height.equalTo(view.frame.height / 10)
            $0.width.equalTo(view.frame.width / 1.4)
        }
        
        viewBeforePrivacyPolicyLabel.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.top.equalTo(privacyPolicyLabelImageView.snp.bottom).inset(view.frame.height / -11.5)
            $0.height.equalTo(view.frame.height / 3.9)
            $0.width.equalTo(view.frame.width / 1.4)
        }
        
        textPrivacyPolicyLabel.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.top.left.bottom.right.equalToSuperview().inset(15)
        }
        
        continueButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(textPrivacyPolicyLabel.snp.bottomMargin).inset(-55)
        }
        
        termsOfUseAndPrivacyPolicyButtons.setupConstraints(superView: view, continueButton: continueButton)
    }
}
