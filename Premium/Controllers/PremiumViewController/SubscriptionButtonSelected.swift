//
//  SubscriptionButtonSelected.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit



struct SubscriptionButtonSelected: SubsButtonConfiguration{
    var backgroundColor: UIColor = SF.Color.subscriptionButtonSelectedBackgroundColor
    var image: UIImage = SF.Image.subscriptionButtonSelectedImage ?? UIImage(systemName: "xmark.app")!
    var colorFont: UIColor = SF.Color.subscriptionButtonSelectedFontColor
}
