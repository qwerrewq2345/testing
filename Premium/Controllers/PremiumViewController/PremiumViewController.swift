//
//  PremiumViewController.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit
import SnapKit

final class PremiumViewController: UIViewController {
    
    // MARK: - UI Elements
    private lazy var backgroundView = BackgroundView()
    private lazy var premiumImageView = UIImageView()
    private lazy var descriptionLabel = UILabel()
    private lazy var goldButton = UIButton()
    private lazy var restorePurchasesButton = UIButton()
    private lazy var termsOfUseAndPrivacyPolicyButtons = TermsOfUseAndPrivacyPolicyButtons()
    private lazy var tableView = UITableView()
    private lazy var closeButton = UIButton()
    
    // MARK: - Property
    private var indecSelectedButton = 0
    private var allSubsButtons: [SubsButton] = []
    
    // MARK: - Life cycle
    override func loadView() {
        view = UIView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtons()
        setupView()
        setupConstraints()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Navigation
private extension PremiumViewController {
    
    @objc func closeButtonPress(){
        print("closeButtonPress")
    }
    
    @objc func openPrivacyPolicy() {
        termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.backgroundColor = .systemIndigo
        UIView.animate(withDuration: 0.01) {
            self.termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.backgroundColor = .clear
        }
        print("openPrivacyPolicy")
    }
    
    @objc func openTermsOfUse() {
        termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.backgroundColor = .systemIndigo
        UIView.animate(withDuration: 0.01) {
            self.termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.backgroundColor = .clear
        }
        print("openTermsOfUse")
    }
    
    @objc func restorePurchases() {
        print("restorePurchasesButton")
    }
}


// MARK: - Layout Setup
private extension PremiumViewController {
    func setupView() {
        view.addSubview(backgroundView)
        
        view.addSubview(closeButton)
        closeButton.setTitle("X", for: .normal)
        closeButton.tintColor = .white
        closeButton.addTarget(self, action: #selector(closeButtonPress), for: .touchUpInside)
        
        view.addSubview(premiumImageView)
        premiumImageView.image = SF.Image.premiumImageViewImage
        premiumImageView.contentMode = .scaleAspectFill
        
        view.addSubview(descriptionLabel)
        descriptionLabel.text = SF.Text.descriptionLabelPremiumVCText
        descriptionLabel.font = SF.Font.descriptionLabelPremiumVCFont(view: view)
        descriptionLabel.textColor = .white
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .left
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(PremiumTableViewCell.self, forCellReuseIdentifier: SF.ID.premiumTableViewCell)
        view.addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.isScrollEnabled = false
        
        view.addSubview(goldButton)
        goldButton.layer.cornerRadius = 8
        goldButton.setImage(SF.Image.goldButtonImage, for: .normal)
        
        view.addSubview(restorePurchasesButton)
        restorePurchasesButton.setImage(SF.Image.restorePurchasesButtonImage, for: .normal)
        restorePurchasesButton.addTarget(self, action: #selector(restorePurchases), for: .touchUpInside)
        
        view.addSubview(termsOfUseAndPrivacyPolicyButtons)
        termsOfUseAndPrivacyPolicyButtons.privacyPolicyButton.addTarget(self, action: #selector(openPrivacyPolicy), for: .touchUpInside)
        termsOfUseAndPrivacyPolicyButtons.termsOfUseButton.addTarget(self, action: #selector(openTermsOfUse), for: .touchUpInside)
    }
    
// MARK: - Layout Constraints
    func setupConstraints(){
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        closeButton.snp.makeConstraints {
            $0.width.height.equalTo(view.frame.height / 37)
            $0.top.equalToSuperview().inset(view.frame.height / 10.5)
            $0.right.equalToSuperview().inset(view.frame.height / 15.5)
        }
        
        premiumImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().inset(view.frame.height / 10.5)
            $0.height.equalTo(view.frame.height / 11.3)
            $0.width.equalTo(view.frame.width / 2)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(premiumImageView.snp.bottom).inset(-24)
            $0.height.equalTo(view.frame.height / 6.3)
            $0.width.equalTo(premiumImageView.snp.width)
        }
        
        tableView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(goldButton.snp.top).inset(-30)
            $0.width.equalTo(view.frame.width / 1.25)
            $0.height.equalTo(view.frame.height / 4.5)
        }
        
        goldButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.width.equalTo(tableView.snp.width)
            $0.bottom.equalTo(restorePurchasesButton.snp.top).inset(-24)
            $0.height.equalTo(view.frame.height / 22)
        }
        
        restorePurchasesButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(termsOfUseAndPrivacyPolicyButtons.snp.top).inset(-17)
            $0.width.equalTo(view.frame.width / 3)
            $0.height.equalTo(10)
        }
        
        termsOfUseAndPrivacyPolicyButtons.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(view.frame.height / 12.5)
            $0.width.equalTo(view.frame.width / 2.3 )
            $0.height.equalTo(view.frame.height / 49)
        }
    }
    
// MARK: - setup SubsButton
    private func setupButtons() {
        let first = SubsButton(durationLabelText: "1 year",
                               priceLabelText: "119,88",
                               pricePerMonthLabelText: "9,99",
                               buttonState: .selected)
        
        let second = SubsButton(durationLabelText: "1 month",
                                priceLabelText: "4,99",
                                pricePerMonthLabelText: "4,99",
                                buttonState: .notSelected)
        
        let third = SubsButton(durationLabelText: "1 week",
                               priceLabelText: "2,99",
                               pricePerMonthLabelText: "11,96",
                               buttonState: .notSelected)
        allSubsButtons = [first, second, third]
    }
}

// MARK: - setup tableView
extension PremiumViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        allSubsButtons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SF.ID.premiumTableViewCell, for: indexPath) as? PremiumTableViewCell
        else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.setup(subsButton: allSubsButtons[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indecSelectedButton == indexPath.row {
            return
        }else{
            indecSelectedButton = indexPath.row
            for i in 0..<allSubsButtons.count {
                let ind = IndexPath(row: i, section: 0)
                if i == indecSelectedButton{
                    guard let cell = tableView.cellForRow(at: ind) as? PremiumTableViewCell else { return }
                    cell.view?.update(buttonState: .selected)
                }else{
                    guard let cell = tableView.cellForRow(at: ind) as? PremiumTableViewCell else { return }
                    cell.view?.update(buttonState: .notSelected)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.frame.height / 3
    }
}
