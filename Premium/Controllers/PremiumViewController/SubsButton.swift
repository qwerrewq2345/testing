//
//  SubsButton.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit
import SnapKit

protocol SubsButtonConfiguration {
    var backgroundColor: UIColor { get }
    var image: UIImage { get }
    var colorFont: UIColor { get }
}

enum SubsButtonState{
    case selected
    case notSelected
}

final class SubsButton: UIView {
    
// MARK: - UI Elements
    let imageView = UIImageView()
    let durationLabel = UILabel()
    let priceLabel = UILabel()
    let stack = UIStackView()
    let pricePerMonthLabel = UILabel()
    
// MARK: - property
    private var durationLabelText: String
    private let priceLabelText: String
    private let pricePerMonthLabelText: String
    private var stateConfig: SubsButtonConfiguration
    
    var buttonState: SubsButtonState
    
    private let button = UIView(frame: .zero)
    
// MARK: - update view after click
    func update(buttonState: SubsButtonState){
        self.buttonState = buttonState
        switch buttonState{
            case .selected: stateConfig = SubscriptionButtonSelected()
            case .notSelected: stateConfig = SubscriptionButtonNotSelected()
        }
        setupView()
    }
    
    
// MARK: - Life cycle
    init(
        durationLabelText: String,
        priceLabelText: String,
        pricePerMonthLabelText: String,
        buttonState: SubsButtonState
    ) {
        self.durationLabelText = durationLabelText
        self.priceLabelText = "\(priceLabelText)" + " $"
        self.pricePerMonthLabelText = "\(pricePerMonthLabelText)" + " $ / month"
        self.buttonState = buttonState
        
        switch buttonState{
            case .selected: stateConfig = SubscriptionButtonSelected()
            case .notSelected: stateConfig = SubscriptionButtonNotSelected()
        }
        
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Layout Setup
private extension SubsButton {
    func setupView() {
        addSubview(button)
        button.backgroundColor = stateConfig.backgroundColor
        button.layer.cornerRadius = 8
        
        button.addSubview(imageView)
        imageView.image = stateConfig.image
    
        durationLabel.text = durationLabelText
        durationLabel.textColor = stateConfig.colorFont
        durationLabel.font = SF.Font.durationLabelFont

        priceLabel.text = priceLabelText
        priceLabel.textColor = stateConfig.colorFont
        priceLabel.font = SF.Font.priceLabelFont
        
        stack.addArrangedSubview(durationLabel)
        stack.addArrangedSubview(priceLabel)
        stack.axis = .vertical
        stack.spacing = 6
        button.addSubview(stack)
        
        button.addSubview(pricePerMonthLabel)
        pricePerMonthLabel.text = pricePerMonthLabelText
        pricePerMonthLabel.textColor = stateConfig.colorFont
        pricePerMonthLabel.font = SF.Font.pricePerMonthLabelFont
        
        
// MARK: - Setup Constraints
        button.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.left.equalTo(22)
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(22)
        }
        
        stack.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(imageView.snp.right).inset(-22)
            $0.width.greaterThanOrEqualTo(68)
        }
        
        pricePerMonthLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.right.equalToSuperview().inset(16)
            $0.height.equalTo(16)
            $0.width.equalTo(113)
        }
    }
}
