//
//  PremiumTableViewCell.swift
//  Premium
//
//  Created by Иван Климов on 24.05.2023.
//

import UIKit
import SnapKit

class PremiumTableViewCell: UITableViewCell{
    var view: SubsButton? = nil
    
    func setup(subsButton: SubsButton) {
        view = subsButton

        guard let view else { return }
        addSubview(view)
        self.backgroundColor = .clear
        view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
