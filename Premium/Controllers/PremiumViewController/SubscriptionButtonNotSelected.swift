//
//  SubscriptionButtonNotSelected.swift
//  Premium
//
//  Created by Иван Климов on 23.05.2023.
//

import UIKit


struct SubscriptionButtonNotSelected: SubsButtonConfiguration{
    var backgroundColor: UIColor = SF.Color.subscriptionButtonSelectedFontColor
    var image: UIImage = SF.Image.subscriptionButtonNotSelectedImage ?? UIImage(systemName: "xmark.app")!
    var colorFont: UIColor = SF.Color.subscriptionButtonSelectedBackgroundColor
}
