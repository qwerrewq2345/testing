//
//  File.swift
//  Premium
//
//  Created by Иван Климов on 24.05.2023.
//

import UIKit

struct SF {
    static let Color = MyColor()
    static let Image = MyImage()
    static let Text = MyText()
    static let Font = MyFont()
    static let ID = MyID()
}

struct MyColor {
//MARK: - PrivacyPolicyViewController
    let textPrivacyPolicyLabelbackgroundColor = UIColor(displayP3Red: 0.18, green: 0.03, blue: 0.37, alpha: 0.6)
    let textPrivacyPolicyLabelTextColor = UIColor.white
    
//MARK: - subs buttons
    let subscriptionButtonSelectedBackgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8)
    let subscriptionButtonSelectedFontColor = UIColor(red: 0.14, green: 0.05, blue: 0.33, alpha: 0.4)
}

//MARK: - Images
struct MyImage {
//MARK: - GeneralElements
    let backgroundImage = UIImage(named: "backgroundView")
    let continueButtonImage = UIImage(named: "continueButton")
    let privacyPolicyButtonImage = UIImage(named: "privacyPolicyButton")
//MARK: - WelcomeViewController
    let welcomeLabelImageViewImage = UIImage(named: "welcomeLabel")
    let centralInfoImageViewImage = UIImage(named: "centralinfo")
    
//MARK: - PrivacyPolicyViewController
    let privacyPolicyLabelImageView = UIImage(named: "PrivacyPolicyLabel")
    
//MARK: - PremiumViewController
    let premiumImageViewImage = UIImage(named: "premiumImageView")
    let goldButtonImage = UIImage(named: "goldenButton")
    let restorePurchasesButtonImage = UIImage(named: "RestorePurchasesButton")
    
    let subscriptionButtonSelectedImage =  UIImage(named: "selectedImage")
    let subscriptionButtonNotSelectedImage = UIImage(named: "noSelectedImage")
}

//MARK: - Texts
struct MyText {
    let textPrivacyPolicyLabelText = "• Device-specific information like OS version and IP dress to optimize our network connection to you. We do not store or log you IP address after you disconnect from the VPN \n\n• Total bandwidth consumed and time connected  to our VPN service"
    let descriptionLabelPremiumVCText = "• The fastest servers \n• Automatic VPN Mode \n• Speed Test \n• Connection Info"
}

//MARK: - Fonts
struct MyFont {    
//MARK: - PrivacyPolicyViewController
    let textPrivacyPolicyLabelFont = UIFont(name: "Sansita-Regular", size: 17.0)
    
//MARK: - PremiumViewController
    func descriptionLabelPremiumVCFont(view: UIView) -> UIFont?{
        UIFont(name: "Sansita-Regular", size: view.frame.height / 39)
    }
    
//MARK: - Fonts subs buttons
    let durationLabelFont = UIFont(name: "OnestRegular", size: 20)
    let priceLabelFont = UIFont(name: "OnestRegular", size: 16)
    let pricePerMonthLabelFont = UIFont(name: "OnestRegular", size: 16)
}

//MARK: - ID
struct MyID {
    let premiumTableViewCell = "Cell"
}
