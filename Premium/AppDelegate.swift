//
//  AppDelegate.swift
//  Premium
//
//  Created by Иван Климов on 22.05.2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let welcomeViewController = UINavigationController(rootViewController: PrivacyPolicyViewController())
        let privacyPolicyViewController = UINavigationController(rootViewController: PremiumViewController())
        let premiumViewController = UINavigationController(rootViewController: WelcomeViewController())
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [premiumViewController, welcomeViewController, privacyPolicyViewController]
        
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
        return true
    }
}

